<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/charts', 'ChartsController@index')->name('chart');
Route::get('/components', 'ComponentsController@index')->name('components');
Route::get('/tables', 'TablesController@index')->name('tables');

Route::get('/map', function(){

    return view('map');
} );
